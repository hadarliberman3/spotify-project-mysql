// require('dotenv').config();
import express from "express";
import morgan from "morgan";
// import log from "marker";
import cors from "cors";
import cron from "node-cron";
import { exec } from "child_process";
import fetch from "node-fetch";

import {connect_db} from "./db/mongoose.connection.js";
import { connect_mysql } from "./db/mysql.connection.config.js";


import artistRouter from "./modules/artist/artist.router.js";
import playlistRouter from "./modules/playlist/playlist.router.js";
import songRouter from "./modules/song/song.router.js";
import userRouter from "./modules/user/user.router.js";
import authRouter from "./modules/auth/auth.router.js";


import { logHttpToFile } from "./middleware/http.logsmw.js";
import {URLnotFoundMW,PrintErrorMW,logToFileErrorMW,ResponseErrorMW} from "./middleware/errors.handler.js";  
import path, { dirname } from "path";
import { fileURLToPath } from "url";

const __dirname = dirname(fileURLToPath(import.meta.url)); 

const { PORT = 3030,HOST = "localhost", DB_URI } = process.env;


class Api{

  LOG_ERR_path:string=path.resolve(__dirname,"../../src/server/logs/errors.logs.log");
  LOG_HTTP_path:string=path.resolve(__dirname,"../../src/server/logs/http.logs.log");
  app = express();

  constructor(){
    this.general();
    this.routing();
    this.httpLogs(this.LOG_HTTP_path);
    this.errorHandleMW(this.LOG_ERR_path);
    this.startServer();
    // this.dbBackup();
  }

  general(){
    this.app.use(cors());
    this.app.use(morgan("dev"));
  }

  routing(){
    this.app.use("/api/users", userRouter);
    this.app.use("/api/playlists", playlistRouter);
    this.app.use("/api/artists", artistRouter);
    this.app.use("/api/songs", songRouter);
    this.app.use("/api", authRouter);


  }
  httpLogs(LOG_HTTP_path:string){
    this.app.use(logHttpToFile(LOG_HTTP_path));
  }

  errorHandleMW(LOG_ERR_path:string){
    this.app.use(URLnotFoundMW);
    this.app.use(PrintErrorMW);
    this.app.use(logToFileErrorMW(LOG_ERR_path));
    this.app.use(ResponseErrorMW);
    this.app.use("*", URLnotFoundMW);
  }

  async dbBackup(){
    try{
    const port=3031;
    cron.schedule("*/20 * * * * *", async () => {
      const child = exec("docker exec mysql-db /usr/bin/mysqldump -u root --password=qwerty spotify");
      await fetch(`http://${HOST}:${port}/backup`, {
      method: "post",
      body: child.stdout,
      // headers: {"Content-Type": "application/json"}
    });
    console.log("scheduler");
  });
  }
  catch(err){
    console.log(err);
  };
}

 async startServer(){

   //connect to mongo db
   try{
    console.log({DB_URI});
    await connect_mysql();
    await connect_db(String(DB_URI));  
    await this.app.listen(Number(PORT),HOST);
    console.log("api is live on",` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`);  
      }catch(err){
     console.log(err);
    }
  
  }
}
const myApi=new Api();



// const app = express();

// // middleware
// app.use(cors());
// app.use(morgan("dev"));

// // routing
// // app.use('/api/stories', story_router);
// app.use(logHttpToFile(LOG_HTTP_path));
// app.use("/api/users", user_router);

// // central error handling
// app.use(URLnotFoundMW);
// app.use(PrintErrorMW);
// app.use(logToFileErrorMW(LOG_ERR_path));
// app.use(ResponseErrorMW);


//when no routes were matched...
// app.use("*", URLnotFoundMW);

//start the express api server
// ;(async ()=> {
//   //connect to mongo db
//   await connect_db(String(DB_URI));  
//   await app.listen(Number(PORT),HOST);
//   console.log("api is live on",` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`);  
// })().catch(console.log);

