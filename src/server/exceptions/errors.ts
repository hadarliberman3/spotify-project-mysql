export class MyErrorException extends Error{
    status:number;
    message:string;

    constructor(status:number, message:string){
    super(message);
    this.status=status;
    this.message=message;
    }
}

export class URLnotFound extends MyErrorException{
    constructor(path:string){
        super(404,`url with path ${path} not found`);
    }
}
