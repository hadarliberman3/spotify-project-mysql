import express from "express";
import { Request,Response } from "express";
import raw from "../../middleware/route.async.wrapper.js";
import cookieParser from "cookie-parser";
import {createPlaylistService,getAllPlaylistsService, getAllMyPlaylistsService,AddSongToPlaylistService,deleteSongFromPlaylistService,deletePlaylistService,updatePlaylistService} from "./playlist.service.js";
import {getUserDetaliesFromAccessToken} from "../auth/auth.service.js";
import { verifyAuthRole } from "../../middleware/auth.middlware.js";
import { RolesEnum as roles} from "../auth/auth.interface.js";

const router = express.Router();
router.use(express.json());
router.use(cookieParser());


//create playlist
router.post("/",verifyAuthRole([roles.user,roles.moderator,roles.admin]), raw(async function createPlaylist(req:Request,res:Response){
    const access_token = req.headers["x-access-token"];
    const {id}=await getUserDetaliesFromAccessToken(access_token as string);
    const newPlaylist = await createPlaylistService(req.body,id);
    res.status(200).json(newPlaylist);

}));

//add song to playlist
router.post("/:playlistId/playlist/:songId", verifyAuthRole([roles.user,roles.moderator,roles.admin]), raw(async function addSongToPlaylist(req:Request,res:Response){
    const updatedPlaylist = await AddSongToPlaylistService(req.params.playlistId,req.params.songId,req.path);
    res.status(200).json(updatedPlaylist);

}));

// get all my playlists
router.get("/",verifyAuthRole([roles.user]), raw(async function getAllPlaylists(req:Request,res:Response){
    const access_token = req.headers["x-access-token"];
    const {id}=await getUserDetaliesFromAccessToken(access_token as string);
    const playlists = await getAllMyPlaylistsService(id);
    res.status(200).json(playlists);

}));

// get all playlists
router.get("/all",verifyAuthRole([roles.moderator,roles.admin]), raw(async function getAllPlaylists(req:Request,res:Response){
    const playlists = await getAllPlaylistsService();
    res.status(200).json(playlists);

}));

//delete my playlist
router.delete("/:id",verifyAuthRole([roles.user]), raw(async function deletePlaylist(req:Request, res:Response){
    const playlist = await deletePlaylistService(req.params.id,req.path);
    res.status(200).json(playlist);
    
}));

//update my playlist
router.put("/:id", raw(async function updatePlaylist(req:Request, res:Response){
    const playlist = await updatePlaylistService(req.params.id,req.body);
    res.status(200).json(playlist);
}));

//delete playlist
// router.delete("/:id",verifyAuthRole([roles.user,roles.moderator,roles.admin]), raw(async function deletePlaylist(req:Request, res:Response){
//     const playlist = await deletePlaylistService(req.params.id,req.path);
//     res.status(200).json(playlist);
    
// }));

//delete song from playlist
router.delete("/:playlistId/playlist/:songId", verifyAuthRole([roles.user,roles.moderator,roles.admin]), raw(async function deleteSongFromPlaylist(req:Request, res:Response){
    const playlist = await deleteSongFromPlaylistService(req.params.playlistId,req.params.songId,req.path);
    res.status(200).json(playlist);
    
}));

export default router;




