
import { URLnotFound } from "../../exceptions/errors.js";
import {createPlaylistDB,getAllMyPlaylistsDB,getAllPlaylistsDB,getOnePlaylistDB,updatePlaylistDB,AddSongToPlaylistDB,deleteSongFromPlaylistDB,deletePlaylistDB} from "./playlist.repository.js";
import { IPlaylistSQL, IEditPlaylistSQL } from "./playlist.interface.js";

export async function createPlaylistService(playlist:IPlaylistSQL,userId:string){
    return await createPlaylistDB(playlist,userId);
}

export async function getAllPlaylistsService (){
    return getAllPlaylistsDB();  
};

export async function getAllMyPlaylistsService (userId:string){
    return getAllMyPlaylistsDB(userId);  
};

export async function getOnePlaylistService(playlistId:string,path:string){
    const playlist = await getOnePlaylistDB(playlistId);
    if (!playlist) throw new URLnotFound(path);
    return playlist;
}

export async function updatePlaylistService(playlistId:string,playlist:IEditPlaylistSQL){
    return await updatePlaylistDB(playlistId,playlist);
}

export async function AddSongToPlaylistService(playlistId:string,songId:string,path:string){
    const playlist=await AddSongToPlaylistDB(playlistId,songId);
    if(!playlist) throw new URLnotFound(path);
    return playlist;
}

export async function deleteSongFromPlaylistService(playlistId:string, songId:string,path:string){
    const playlist=await deleteSongFromPlaylistDB(playlistId,songId);
    if (!playlist) throw new URLnotFound(path);
    return playlist;

}

export async function deletePlaylistService(playlistId:string, path:string){
        const playlist = await deletePlaylistDB(playlistId);
        if (!playlist) throw new URLnotFound(path);

        return playlist;

}


