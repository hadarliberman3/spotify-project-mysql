import {IPlaylistSQL,IEditPlaylistSQL} from "./playlist.interface.js";
import { connection } from "../../db/mysql.connection.config.js";


export async function createPlaylistDB(playlist:IPlaylistSQL,userId:string){

    const sql=`INSERT INTO playlist (name,user_id) VALUES('${playlist.name}','${userId}')`;
    try {
        const [rows]=await connection.query(sql);
        return rows;   
    }
    catch(err){
        console.log(err);
        return null;
    }
}

export async function getAllMyPlaylistsDB (userId:string){
    const sql=`SELECT p.name as "Playlist Name",s.name as "Song Name" FROM playlist as p INNER JOIN playlist_song as ps ON ps.playlist_id=p.id INNER JOIN song as s ON s.id=ps.song_id WHERE p.user_id=${userId}`; 
    try {
       
        const [rows] = await connection.query(sql);
        return rows;
        
    }
    catch(err){
        console.log(err);
        return null;
    } 
};

export async function getAllPlaylistsDB (){
    const sql="SELECT p.name as 'Playlist Name',s.name as 'Song Name' FROM playlist as p INNER JOIN playlist_song as ps ON ps.playlist_id=p.id INNER JOIN song as s ON s.id=ps.song_id"; 
    try {
       
        const [rows] = await connection.query(sql);
        return rows;
        
    }
    catch(err){
        console.log(err);
        return null;
    } 
};

export async function getOnePlaylistDB(playlistId:string){
    const sql=`SELECT * FROM playlist WHERE id=${playlistId}`; 
    try {
        
        const [rows] = await connection.query(sql);
        return rows;
        
    }
    catch(err){
        console.log(err);
        return null;
    } 
}

 export async function updatePlaylistDB(playlistId:string,playlist:IEditPlaylistSQL){
    const sql=`UPDATE playlist SET name='${playlist.name}' WHERE id=${playlistId}`; 
    try {
  
        const [rows] = await connection.query(sql);
        return rows;
        
    }
    catch(err){
        console.log(err);
        return null;
    } 
 }

 export async function AddSongToPlaylistDB(playlist_id:string,song_id:string){
    const sql="INSERT INTO playlist_song (playlist_id,song_id) VALUES('?','?')"; 
    try {
        
        const [rows] = await connection.query(sql,{
            playlist_id,
            song_id
        });
        return rows;
        
    }
    catch(err){
        console.log(err);
        return null;
    }   
 }

export async function deleteSongFromPlaylistDB(playlistId:string, songId:string){
    const sql=`DELETE FROM playlist_song WHERE song_id='${songId}' AND playlist_id='${playlistId}'`; 
    try {
       
        const [rows] = await connection.query(sql,[songId,playlistId]);
        return rows;
        
    }
    catch(err){
        console.log(err);
        return null;
    } 

}

export async function deletePlaylistDB(playlistId:string){
    const sql=`DELETE FROM playlist_song WHERE playlist_id='${playlistId}'`;
    const sql2=`DELETE FROM playlist WHERE id='${playlistId}'`; 
    try {
      
        const [rows] = await connection.query(sql);
        await connection.query(sql2);
        return rows;
        
    }
    catch(err){
        console.log(err);
        return null;
    }     

}

