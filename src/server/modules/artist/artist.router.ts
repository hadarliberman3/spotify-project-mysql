import express from "express";
import { Request,Response } from "express";
import raw from "../../middleware/route.async.wrapper.js";
import {createArtistService, getAllMyArtistsService,getOneArtistService,
    updateArtistService,deleteArtistService,getAllArtistsService} from "./artists.service.js";
import { getUserDetaliesFromAccessToken } from "../auth/auth.service.js";
import { verifyAuthRole } from "../../middleware/auth.middlware.js";
import cookieParser from "cookie-parser";

import {RolesEnum as role} from "../auth/auth.interface.js";
import {updateStatusService} from "../auth/auth.service.js";

const router = express.Router();
router.use(express.json());
router.use(cookieParser());

const TABLE_NAME="artist";

//create artist
router.post("/", verifyAuthRole([role.user,role.moderator,role.admin]), raw(async function createArtist(req:Request,res:Response){
    const access_token = req.headers["x-access-token"];
    const {id,roles}=await getUserDetaliesFromAccessToken(access_token as string);
    const newArtist=await createArtistService(req.body,id,roles);
    res.status(200).json(newArtist);

}));


router.get("/all", verifyAuthRole([role.moderator,role.admin]), raw(async function getAllArtists(req:Request,res:Response){
    const artists = await getAllArtistsService();
    res.status(200).json(artists);

}));

router.get("/", verifyAuthRole([role.user]), raw(async function getAllMyArtists(req:Request,res:Response){
    const access_token = req.headers["x-access-token"];
    const {id,roles}=await getUserDetaliesFromAccessToken(access_token as string);
    const artists = await getAllMyArtistsService(id);
    res.status(200).json(artists);

}));


//get one artist
router.get("/:id", verifyAuthRole([role.user,role.moderator,role.admin]), raw(async function getOneArtist (req:Request, res:Response){
    const artist = await getOneArtistService(req.params.id,req.path);
    res.status(200).json(artist);
}));

router.put("/:id", verifyAuthRole([role.user,role.moderator,role.admin]), raw(async function updateArtist(req:Request, res:Response){
    const artist = await updateArtistService(req.params.id,req.body,req.path);
    res.status(200).json(artist);
}));

router.delete("/:id",verifyAuthRole([role.moderator,role.admin]), raw(async function deleteArtist(req:Request, res:Response){
    const artist = await deleteArtistService(req.params.id,req.path);
    res.status(200).json(artist);
    
}));

//update artist status
router.put("/:artistId/:statusId",verifyAuthRole([role.moderator,role.admin]), raw(async function updateStatusArtist(req:Request, res:Response){
    console.log("update status service");
     const song = await updateStatusService(TABLE_NAME,req.params.artistId,req.params.statusId);
     res.status(200).json(song);
 }));



export default router;