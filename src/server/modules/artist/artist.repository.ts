
import { IArtistSQL,IEditArtistSQL } from "./artist.interface.js";
import { connection } from "../../db/mysql.connection.config.js";


export async function createArtistDB(artist:IArtistSQL,user_id:string){
    const sql="INSERT INTO artist SET ?";
    try {
      
        const [rows]=await connection.query(sql,{
            first_name:artist.first_name,
            last_name:artist.last_name,
            status_id:artist.status_id,
            user_id
        });
        return rows;   
    }
    catch(err){
        console.log(err);
        return null;
    }
      
}





export async function getAllMyArtistsDB (userId:string){
    const sql="SELECT * FROM artist as a "+
    "INNER JOIN status as s ON s.id=a.status_id "+
    "WHERE s.id='1' AND a.user_id = ? ";
    try {
   
        const [rows] = await connection.query(sql,userId);
        return rows;
        
    }
    catch(err){
        console.log(err);
        return null;
    } 
}

export async function getAllArtistsDB (){
    const sql="SELECT * FROM artist";
    try {
   
        const [rows] = await connection.query(sql);
        return rows;
        
    }
    catch(err){
        console.log(err);
        return null;
    } 
}

export async function getOneArtistDB(artistId:string){
    const sql=`SELECT * FROM artist WHERE id=${artistId}`; 
    try {
       
        const [rows] = await connection.query(sql);
        return rows;
        
    }
    catch(err){
        console.log(err);
        return null;
    } 
   
}

export async function updateArtistDB(artistId:string,artist:IEditArtistSQL){
    const artistBeforeUpdate=(await getArtistByIdDB(artistId) as any)[0];
    const {
        first_name=artistBeforeUpdate.first_name,
        last_name=artistBeforeUpdate.last_name
    } = artist;
    const sql="UPDATE artist SET first_name=?, last_name=? WHERE id=?"; 
    try {
     
        const [rows] = await connection.query(sql,[first_name,last_name,artistId]);
        return rows;
        
    }
    catch(err){
        console.log(err);
        return null;
    } 
}


export async function deleteArtistDB(artistId:string){

    const deleteSongSql=`DELETE FROM song WHERE artist_id='${artistId}'`;
    const deleteArtistSql=`DELETE FROM artist WHERE id='${artistId}'`; 
    try {
     
        await connection.query(deleteArtistSql);
        const [rows] = await connection.query(deleteSongSql);
        return rows;
        
    }
    catch(err){
        console.log(err);
        return null;
    }     
        
}

export async function getArtistByIdDB(artistId:string){
    const sql=`SELECT * FROM artist WHERE id=${artistId}`; 
    try {
       
        const [rows] = await connection.query(sql);
        return rows;
        
    }
    catch(err){
        console.log(err);
        return null;
    } 
   
}
