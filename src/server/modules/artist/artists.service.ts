
import { IArtistSQL, IEditArtistSQL} from "./artist.interface.js";
import { URLnotFound } from "../../exceptions/errors.js";
import { deleteArtistDB } from "../../db/repository.artist.js";
import {createArtistDB,getOneArtistDB,updateArtistDB, getAllArtistsDB,getAllMyArtistsDB} from "./artist.repository.js";
import { RolesEnum as role, StatusEnum } from "../auth/auth.interface.js";


export async function createArtistService(artist:IArtistSQL,userId:string,roles:string[]){
    if(roles.includes(role.moderator|| role.admin)){
        artist.status_id=StatusEnum.active;
        console.log("active"+artist.status_id);
    }
    else if(roles.includes(role.user)){
        artist.status_id=StatusEnum.pending;
        console.log("pending"+artist.status_id);
    }
    return await createArtistDB(artist,userId);
}


export async function getAllArtistsService (){
    return await getAllArtistsDB();   
}
export async function getAllMyArtistsService (userId:string){
    return await getAllMyArtistsDB(userId);   
}


export async function getOneArtistService(artistId:string,path:string){
    const artist = await getOneArtistDB(artistId);
    if (!artist) throw new URLnotFound(path);
    return artist;
}

export async function updateArtistService(artistId:string,artist:IEditArtistSQL,path:string){
    const updatedArtist=await updateArtistDB(artistId,artist);
    if(!updatedArtist) throw new URLnotFound(path);
    return updatedArtist;
}


export async function deleteArtistService(artistId:string, path:string){
    const artist = await deleteArtistDB(artistId);
    if (!artist) throw new URLnotFound(path);
    return artist;
}