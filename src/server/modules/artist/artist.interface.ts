import internal from "stream";

export interface Iartist{
    first_name:string,
    last_name:string,
    songs:string[]
}

// export type IeditArtist=Pick<Iartist, "first_name"|"last_name"|"songs">;
export interface IeditArtist{
    first_name?:string,
    last_name?:string,
    songs?:string[]
}

export interface IArtistSQL{
    id:number,
    first_name:string,
    last_name:string,
    status_id:string,
    user_id:number
}


export interface IEditArtistSQL{
    id:number,
    first_name:string,
    last_name:string,
    status_id:number,
    user_id:number
}