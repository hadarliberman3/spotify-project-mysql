import { connection } from "../../db/mysql.connection.config.js";

export async function updateStatusDB(tableName:string,entetieId:string,statusId:string){
    console.log(tableName,entetieId,statusId);
    const sql=`UPDATE ${tableName} SET status_id=? WHERE id=?`; 
    try {
        const [rows] = await connection.query(sql,[statusId,entetieId]);
        return rows;
        
    }
    catch(err){
        console.log(err);
        return null;
    }

}