import { IuserSQL } from "../user/user.interface.js";
import { deleteRefreshTokenDB } from "../user/user.repository.js";
import jwt from "jsonwebtoken";
import ms from "ms";
import { getUser } from "../../db/repository.auth.js";
import { URLnotFound } from "../../exceptions/errors.js";
const { CLIENT_ORIGIN, APP_SECRET, ACCESS_TOKEN_EXPIRATION, REFRESH_TOKEN_EXPIRATION } = process.env;
import bcrypt from "bcryptjs";
import { getUserFromDB } from "../user/user.repository.js";
import { JwtPayload } from "jsonwebtoken";
import {updateStatusDB} from "./auth.repository.js";



export async function createTokens(user: IuserSQL) {
    const accessToken = jwt.sign({ id: user.id, roles:user.roles }, APP_SECRET as string, {
        expiresIn: ACCESS_TOKEN_EXPIRATION
    });

    const refreshToken = jwt.sign({ id: user.id, roles:user.roles }, APP_SECRET as string, {
        expiresIn: REFRESH_TOKEN_EXPIRATION // expires in 60 days... long-term... 
    });
    return { "refreshToken": refreshToken, "accessToken": accessToken };
}

export async function deleteRefreshTokenService(userId: string) {
    await deleteRefreshTokenDB(userId);
}

export async function loginService(user: IuserSQL, path: string) {
    
    const { email, password } = user;
    const userDB = await getUserFromDB(email as string) as any;
    if (!userDB) throw new URLnotFound(path);
    const correctPassword = await bcrypt.compare(password as string, userDB[0].password);

    return correctPassword ? userDB : false;
}

export async function getUserDetaliesFromAccessToken(access_token:string) {
    const decoded = await jwt.verify(access_token, APP_SECRET as string);
    // console.log({ decoded });
    let {id,roles} = decoded as JwtPayload;
    roles=roles.split(",");
    return {id,roles};
   
}

export async function updateStatusService(tableName:string,entetieId:string,statusId:string) { 
    console.log("update statuse before db"); 
    const update= await updateStatusDB(tableName,entetieId,statusId);
    return update;
   
}


export async function getRolesFromAccessToken(access_token:string){
    const decoded = await jwt.verify(access_token, APP_SECRET as string);
    console.log({ decoded });
    let {roles} = decoded as JwtPayload;
    roles=roles.split(",");
    console.log(roles);
    return roles; 
}


