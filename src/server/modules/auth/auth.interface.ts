export enum RolesEnum {
    guest = "1",
    user = "2",
    moderator="3",
    admin="4"
  }

  export enum StatusEnum {
    active = "1",
    rejected = "2",
    pending="3"
 
  }