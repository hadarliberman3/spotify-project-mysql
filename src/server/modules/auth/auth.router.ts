import express, { NextFunction } from "express";
import raw from "../../middleware/route.async.wrapper.js";
import { Request, Response } from "express";

import jwt, { JwtPayload } from "jsonwebtoken";
import cookieParser from "cookie-parser";
import ms from "ms";
import { registerUserService, updateUserService } from "../user/user.service.js";
import { createTokens, deleteRefreshTokenService, loginService } from "./auth.service.js";
import { IuserSQL,IeditUserSQL } from "../user/user.interface.js";

const { APP_SECRET, ACCESS_TOKEN_EXPIRATION } = process.env;
const router = express.Router();
router.use(express.json());
router.use(cookieParser());


router.post("/login", raw(async function login(req: Request, res: Response) {

    const userDB= await loginService(req.body, req.path) as any;
    console.log(userDB);

    if (userDB) {
        const user=userDB[0];
        console.log(user);
        const { accessToken, refreshToken } = await createTokens(user);
        user.refresh_token = refreshToken;
        console.log(user);
        await updateUserService(user.id as string, user, req.path);
        res.cookie("refresh_token", refreshToken, {
            maxAge: ms("60d"), //60 days
            httpOnly: true
        });

        const payload = { ...user };
        res.status(200).json({
            status: "you are login",
            payload,
            accessToken,
        });
    }
    else {
        res.status(403).json({
            status: "Unauthorized",
            payload: "wrong email or password"
        });
    }

}));

router.post("/register", async function (req: Request, res: Response) {
    const user = req.body;
    const userDB:any = await registerUserService(user);
    if(userDB=="email exists") return res.status(401).json({
        status: "email exists"
    });
    if (!userDB) throw new Error();
    res.status(200).json({ user });
});

router.get("/get-access-token", raw(async (req: Request, res: Response) => {

    const { refresh_token } = req.cookies;
    console.log({ refresh_token });

    if (!refresh_token) return res.status(403).json({
        status: "Unauthorized",
        payload: "No refresh_token provided."
    });


    try {
        // verifies secret and checks expiration
        const decoded = await jwt.verify(refresh_token, APP_SECRET as string);
        if (decoded) {
            console.log({ decoded });


            //check user refresh token in DB
            const { id, profile } = decoded as JwtPayload;

            const access_token = jwt.sign({ id }, APP_SECRET as string, {
                expiresIn: ACCESS_TOKEN_EXPIRATION //expires in 1 minute
            });
            res.status(200).json({ access_token, profile });
        }
    } catch (err) {
        console.log("error: ", err);
        return res.status(401).json({
            status: "Unauthorized",
            payload: "Unauthorized - Failed to verify refresh_token."
        });
    }
}));

router.post("/logout/:userId", async (req: Request, res: Response) => {
    const userId = req.params.userId;
    await deleteRefreshTokenService(userId);
    res.clearCookie("refresh_token");
    res.status(200).json({ status: "You are logged out" });
});

export default router;
