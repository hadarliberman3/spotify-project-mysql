export interface Iuser{
    _id?:string,
    nickname:string,
    password:string,
    playlist_list:string[],
    refresh_token?:string
}

export interface IeditUser{
    _id?:string,
    nickname?:string,
    password?:string,
    playlist_list?:string[]
}

export interface IuserSQL{
    id?:string,
    nickname?:string,
    email?:string,
    password?:string,
    refresh_token?:string,
    roles?:string
}

export interface IeditUserSQL{
    id?:string,
    nickname?:string,
    email?:string,
    password?:string,
    refresh_token?:string,
    roles?:string
}