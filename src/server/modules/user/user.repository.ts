
import mysql from "mysql2/promise";
import { IuserSQL, IeditUserSQL } from "./user.interface.js";
import { config } from "../../db/mysql.connection.config.js";
import { IEditsongSQL } from "../song/song.interface.js";
import {connection} from "../../db/mysql.connection.config.js";


export async function registerUserDB(user:IuserSQL){
    const uniqe:boolean|null=await isUniqeEmail(user.email as string);
    
    if(uniqe===false) return "email exists";
    const sql="INSERT INTO user SET ?";
    try {
      
        const [rows]=await connection.query(sql,
        {
        nickname:user.nickname,
        email:user.email,
        password:user.password,
        roles:user.roles});
        return rows;   
    }
    catch(err){
        console.log(err);
        return null;
    }
     
}

export async function getUserFromDB(userEmail:string){

    const sql="SELECT * FROM user WHERE email=?";
    try {
        console.log(sql,userEmail);
        const [rows]=await connection.query(sql,userEmail);
        console.log(rows);
        return rows;   
    }
    catch(err){
        console.log(err);
        return null;
    } 
}

// export async function getAllUsersDB (){
//     return await userModel.find()
//     .populate("playlist","-_id name");
    
// }


export async function updateUserDB(userId:string,user:IuserSQL){
    const userBeforeUpdate=(await getUserByIdDB(userId) as any)[0];

    const {
        nickname=userBeforeUpdate.nickname,
        email=userBeforeUpdate.email,
        roles=userBeforeUpdate.roles
         } = user;
         console.log(nickname,email,roles);
    
    const sql="UPDATE user SET nickname=?,email=?,roles=? WHERE id=?";
    try {
        const [rows]=await connection.query(sql,[nickname,email,roles,userId]);
        return rows;   
    }
    catch(err){
        console.log(err);
        return null;
    } 
}

export async function deleteRefreshTokenDB(userId:string){
    
    const sql=`UPDATE user SET refresh_token=NULL WHERE id=${userId}`;
    try {
        const [rows]=await connection.query(sql);
        return rows;   
    }
    catch(err){
        console.log(err);
        return null;
    } 

}



export async function getUserProfileDB(userId:string){
    const sql="SELECT u.nickname, u.email, r.role FROM user as u INNER JOIN roles as r ON r.id=u.roles WHERE u.id=?";
    try {
        const [rows]=await connection.query(sql,userId);
        return rows;   
    }
    catch(err){
        console.log(err);
        return null;
    }  
}

export async function isUniqeEmail(email:string){

    const sql=`SELECT * FROM user WHERE email='${email}'`;
    try {
        const [rows]=await connection.query(sql);
        console.log(rows);
        return rows?true:false;
        
    }
    catch(err){
        console.log(err);
        return null;
    } 

}



export async function deleteUserDB(userId:string){
    const playlistSongDeleteSql=`DELETE FROM playlist_song WHERE playlist_id='${userId}'`;
    const playlistDeleteSql=`DELETE FROM playlist WHERE user_id='${userId}'`;

    try {
        const [rows]=await connection.query(playlistDeleteSql);
        await connection.query(playlistSongDeleteSql);

        console.log(rows);
        return rows?true:false;
        
    }
    catch(err){
        console.log(err);
        return null;
    } 
}


    export async function getAllUsersServiceDB(){
        const sql="SELECT * FROM user";
        try {
            const [rows]=await connection.query(sql);
        
            return rows?true:false;
            
        }
        catch(err){
            console.log(err);
            return null;
        } 
    }

    export async function getUserByIdDB(userId:string){
        const sql="SELECT * FROM user where id= ?";
        try {
            const [rows]=await connection.query(sql,userId);
        
            return rows;
            
        }
        catch(err){
            console.log(err);
            return null;
        } 
    }


