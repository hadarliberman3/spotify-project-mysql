import cookieParser from "cookie-parser";
import express from "express";
import { Request,Response } from "express";
import raw from "../../middleware/route.async.wrapper.js";
import { updateUserService, getUserProfileService, getAllUsersService} from "./user.service.js";
import {getUserDetaliesFromAccessToken} from "../auth/auth.service.js";
import { deleteUserService } from "./user.service.js";
import { verifyAuthRole } from "../../middleware/auth.middlware.js";
import { RolesEnum as roles} from "../auth/auth.interface.js";


const router = express.Router();
router.use(express.json());
router.use(cookieParser());


//update my profile
router.put("/", verifyAuthRole([roles.user]), raw(async function updateUser(req:Request, res:Response){
    const access_token = req.headers["x-access-token"];
    const {id}=await getUserDetaliesFromAccessToken(access_token as string);
    const User = await updateUserService(id,req.body,req.path);
    res.status(200).json(User);
}));

//get my profile
router.get("/",verifyAuthRole([roles.user]), raw(async function getUserProfile(req:Request,res:Response){
    const access_token = req.headers["x-access-token"];
    const {id}=await getUserDetaliesFromAccessToken(access_token as string);
    const user = await getUserProfileService(id,req.path);
    res.status(200).json(user);
}));

//get a user by id
router.get("/:userId",verifyAuthRole([roles.moderator,roles.admin]), raw(async function getUserProfile(req:Request,res:Response){
    const user = await getUserProfileService(req.params.userId,req.path);
    res.status(200).json(user);
}));

//delete user
router.delete("/:userId", verifyAuthRole([roles.moderator,roles.admin]), raw(async function deleteUser(req:Request,res:Response){
    const deletedUser=await deleteUserService(req.params.userId,req.path);
    res.status(200).json(deletedUser);
}));

//get all users
router.get("/all", verifyAuthRole([roles.moderator,roles.admin]), raw(async function getAllUsers(req:Request,res:Response){
    const users=await getAllUsersService(req.path);
    res.status(200).json(users);
}));


export default router;
