
import { IuserSQL, IeditUserSQL} from "./user.interface.js";
import { URLnotFound } from "../../exceptions/errors.js";

import { registerUserDB,updateUserDB,getUserProfileDB,deleteUserDB,getAllUsersServiceDB } from "./user.repository.js";
import bcrypt from "bcryptjs";


//register
export async function registerUserService(user:IuserSQL){
    const hash= await bcrypt.hash(user.password as string, 8);
    user.password=String(hash);
    return await registerUserDB(user);
    
   
}

//update user
export async function updateUserService(userId:string,user:IuserSQL,path:string){
    const newUser=await updateUserDB(userId,user);
    if(!newUser) throw new URLnotFound(path);
    return newUser;
}

//get profile
export async function getUserProfileService(userId:string,path:string){
    const user=await getUserProfileDB(userId);
    if(!user) throw new URLnotFound(path);
    return user;
    
}

//delete user
export async function deleteUserService(userId:string, path:string){
    const user = deleteUserDB(userId);
    if (!user) throw new URLnotFound(path);
    return user;
}

export async function getAllUsersService(path:string){
    const user=await getAllUsersServiceDB();
    if(!user) throw new URLnotFound(path);
    return user;
    
}

