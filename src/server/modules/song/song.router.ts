import express from "express";
import { Request,Response } from "express";
import raw from "../../middleware/route.async.wrapper.js";
import {createSongService,getAllSongsService,getOneSongService,
    updateSongService,deleteSongService} from "./song.service.js";
import cookieParser from "cookie-parser";
import {getUserDetaliesFromAccessToken,updateStatusService} from "../auth/auth.service.js";
import { RolesEnum } from "../auth/auth.interface.js";

import {verifyAuthRole } from "../../middleware/auth.middlware.js";

const router = express.Router();
router.use(express.json());
router.use(cookieParser());

const TABLE_NAME="song";


//create song
router.post("/",verifyAuthRole([RolesEnum.user,RolesEnum.moderator,RolesEnum.admin]), raw(async function createSong(req:Request,res:Response){
    const access_token = req.headers["x-access-token"];
    const {id,roles}=await getUserDetaliesFromAccessToken(access_token as string);
    const newsong = await createSongService(req.body,id,roles);
    res.status(200).json(newsong);
}));

//get all songs that create by user
router.get("/", raw(async function getAllSongs(req:Request,res:Response){
    const access_token = req.headers["x-access-token"];
    const {id}=await getUserDetaliesFromAccessToken(access_token as string);
    const songs = await getAllSongsService(id);
    res.status(200).json(songs);
}));


//get one specific song
router.get("/:id", raw(async function getOneSong (req:Request, res:Response){
    const song = await getOneSongService(req.params.id,req.path);
    res.status(200).json(song);
}));

//update song
router.put("/:songId",verifyAuthRole([RolesEnum.user,RolesEnum.moderator,RolesEnum.admin]), raw(async function updateSong(req:Request, res:Response){
    const access_token = req.headers["x-access-token"];
    const {roles}=await getUserDetaliesFromAccessToken(access_token as string);
    const song = await updateSongService(req.params.songId,req.body,roles,req.path);
    res.status(200).json(song);
}));


//delete song
router.delete("/:songId",verifyAuthRole([RolesEnum.moderator,RolesEnum.admin]), raw(async function deleteSong(req:Request, res:Response){
    const song = await deleteSongService(req.params.id,req.path);
    res.status(200).json(song);
    
}));

//update song status
router.put("/:songId/:statusId",verifyAuthRole([RolesEnum.moderator,RolesEnum.admin]), raw(async function updateStatusSong(req:Request, res:Response){
   console.log("update status service");
    const song = await updateStatusService(TABLE_NAME,req.params.songId,req.params.statusId);
    res.status(200).json(song);
}));

// router.post("/:songId/artist/:artistId", raw(async function addArtistToSong(req:Request,res:Response){
//     const connectedSong=await addArtistToSongService(req.params.songId,req.params.artistId);
//     res.status(200).json(connectedSong);
// }));

export default router;
