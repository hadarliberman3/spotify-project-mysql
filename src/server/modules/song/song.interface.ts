export interface Isong{
    name:string,
    singer: string
}

export interface IeditSong{
    name?:string,
    singer?: string
}

export interface IsongSQL{
    id:number,
    name:string,
    status_id:string,
    artist_id:string,
    user_id:string
}

export interface IEditsongSQL{
    id:number,
    name:string,
    status_id:string,
    artist_id:string,
    user_id:string
}