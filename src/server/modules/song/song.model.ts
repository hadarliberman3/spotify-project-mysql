import mongoose from "mongoose";
const { Schema , model  } = mongoose;

export const SongSchema = new Schema({
    name: {type: String,required: true},
    genre: {type: String,required: true },
    artist: {type: Schema.Types.ObjectId, ref:"artist"},
    playlist:[{type: Schema.Types.ObjectId, ref:"playlist"}]
}, {timestamps:true});


export default model("song", SongSchema);
