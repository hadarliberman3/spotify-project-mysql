import mysql from "mysql2/promise";
import { config } from "../../db/mysql.connection.config.js";
import { IsongSQL,IEditsongSQL} from "./song.interface.js";
import { connection } from "../../db/mysql.connection.config.js";

export async function createSongDB(song:IsongSQL,userId:string){

    const sql="INSERT INTO song SET ?";
    try {

        const [rows]=await connection.query(sql,{
            name:song.name,
            status_id:song.status_id,
            artist_id:song.artist_id,
            user_id:userId
        });
        return rows;   
    }
    catch(err){
        console.log(err);
        return null;
    }
}

export async function getAllSongsDB (userId:string){
    const sql="SELECT s.name as song_name ,a.first_name as artist_first_name,"+
              "a.last_name as artist_last_name FROM song as s "+
              "INNER JOIN artist as a ON a.id=s.artist_id "+
              "WHERE s.user_id='?' AND s.status_id='1'";
    
    try {
        const [rows] = await connection.query(sql,userId);
        return rows;
        
    }
    catch(err){
        console.log(err);
        return null;
    } 
    
}

export async function getOneSongDB(songId:string){
    const sql=`SELECT s.name as song_name ,a.first_name as artist_first_name, a.last_name as artist_last_name FROM song as s INNER JOIN artist as a ON a.id=s.artist_id WHERE s.id='${songId}'`; 
    try {
        const [rows] = await connection.query(sql);
        return rows;
        
    }
    catch(err){
        console.log(err);
        return null;
    } 
}

export async function updateSongDB(songId:string,song:IEditsongSQL){
    const songBeforeUpdate= (await getSongByIdDB(songId) as any)[0];
    const {
        name=songBeforeUpdate.name,
        status_id=songBeforeUpdate.status_id,
        artist_id=songBeforeUpdate.artist_id
    } = song;
    const sql="UPDATE song SET name=?, status_id=?, artist_id=? WHERE id=?"; 
    try {
        const [rows] = await connection.query(sql,[name,status_id,artist_id,songId]);
        return rows;
        
    }
    catch(err){
        console.log(err);
        return null;
    }
}

export async function getSongByIdDB(songId:string){
    const sql="SELECT * FROM song WHERE id=?"; 
    try {
        const [rows] = await connection.query(sql,songId);
        return rows;
        
    }
    catch(err){
        console.log(err);
        return null;
    }
}

export async function deleteSongDB(songId:string){
    const sql="DELETE FROM song WHERE id=?"; 
    try {
        const [rows] = await connection.query(sql,songId);
        return rows;
        
    }
    catch(err){
        console.log(err);
        return null;
    }
       
}
