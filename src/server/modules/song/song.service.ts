
import { IsongSQL, IEditsongSQL} from "./song.interface.js";
import { URLnotFound } from "../../exceptions/errors.js";
import {StatusEnum,RolesEnum } from "../auth/auth.interface.js";

import { createSongDB, getAllSongsDB,getOneSongDB,updateSongDB,deleteSongDB } from "./song.repository.js";



export async function createSongService(song:IsongSQL,userId:string,roles:string[]){
    if(roles.includes(RolesEnum.moderator)){
        song.status_id=StatusEnum.active;
    }
    else if(roles.includes(RolesEnum.user)){
        song.status_id=StatusEnum.pending;
    }
    return await createSongDB(song,userId);
}

export async function getAllSongsService (userId:string){
    return await getAllSongsDB(userId);   
}

export async function getOneSongService(songId:string,path:string){
    const song = await getOneSongDB(songId);
    if(!song) throw new URLnotFound(path);
    return song;
}

export async function updateSongService(songId:string,song:IEditsongSQL,roles:string[],path:string){
    if(roles.includes(RolesEnum.moderator)){
        song.status_id=StatusEnum.active;
    }
    else if(roles.includes(RolesEnum.user)){
        song.status_id=StatusEnum.pending;
    }
    const newSong=await updateSongDB(songId,song);
    if(!newSong) throw new URLnotFound(path);
    return newSong;
}

export async function deleteSongService(songId:string, path:string){
    const song = await deleteSongDB(songId);
    if (!song) throw new URLnotFound(path);
    return song;
}
