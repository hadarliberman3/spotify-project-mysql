import playlistModel from "../modules/playlist/playlist.model.js";
import songModel from "../modules/song/song.model.js";
import { Iplaylist, IeditPlaylist } from "../modules/playlist/playlist.interface.js";



export async function createPlaylistDB(playlist:Iplaylist){
    const newPlaylist= await playlistModel.create(playlist);
    if("songs" in newPlaylist){
        const pending = playlist.songs.map(async (current:any) => {
            const song = await songModel.findById(current);
            song.songs.push(newPlaylist);
            await song.save();
        });
        await Promise.all(pending);
    }
    return newPlaylist;
}

export async function getAllPlaylistsDB (){
    return await playlistModel.find().select("name songs geners users");   
};

export async function getOnePlaylistDB(playlistId:string){
    const playlist = await playlistModel.findById(playlistId);
    for(const curSongId of playlist.songs){
        console.log(curSongId._id);
    }
    return playlist;
}

export async function updatePlaylistDB(playlistId:string,playlist:IeditPlaylist){
    return await playlistModel.findByIdAndUpdate(playlistId,playlist,
        {new: true, upsert: false });
}

export async function deleteSongFromPlaylistDB(playlistId:string, songId:string){
    const playlist=await playlistModel.findById(playlistId);
    const song=await songModel.findById(songId);

//delete song from plalyist
    playlist.songs=playlist.songs.filter((curSong:any)=>{
        console.log(curSong._id,songId);
        return curSong._id!=songId;
    });
    await playlist.save();   

//delete playlistId from song
    song.playlist=song.playlist.filter((curId:string)=>curId!=playlistId);
    await song.save();
    return playlist;

}

export async function deletePlaylistDB(playlistId:string){
        let playlist = await playlistModel.findById(playlistId);

        //delete playlistId from song
        for(const curSong of playlist.songs){
            const song= await songModel.findById(curSong._id);
            song.playlist=song.playlist.filter((curId:string)=>curId!=playlistId);
            await song.save();
             
        }
        //delete playlist
        playlist = await playlistModel.findByIdAndRemove(playlistId);

        return playlist;

}

export async function AddSongToPlaylistDB(playlistId:string, songId:string){
    const playlist = await playlistModel.findById(playlistId);
    const song= await songModel.findById(songId);

    //add song to pallyist
    if(!playlist.songs.includes(songId)){
        playlist.songs.push(song);
        await playlist.save();
    }

    //add playlistId to song
    if(!song.playlist.includes(playlistId)){
        song.playlist.push(playlistId);
        await song.save();
    }
    return playlist;
}

