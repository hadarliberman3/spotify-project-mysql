import artistModel from "../modules/artist/artist.model.js";
import songModel from "../modules/song/song.model.js";
import playlistModel from "../modules/playlist/playlist.model.js";
import { Iartist,IeditArtist } from "../modules/artist/artist.interface.js";


export async function createArtistDB(artist:Iartist){
    return await artistModel.create(artist);
}

export async function getAllArtistsDB (){
    return await artistModel.find().select("_id first_name last_name songs");   
}

export async function getOneArtistDB(artistId:string){
    return await artistModel.findById(artistId);
}

export async function updateArtistDB(artistId:string,artist:IeditArtist){
    return await artistModel.findByIdAndUpdate(artistId,artist,{new: true, upsert: false });
}


export async function deleteArtistDB(artistId:string){
        const artist = await artistModel.findById(artistId);

        for(const curSongId of artist.songs){
            const song=await songModel.findById(curSongId);

        //delete songs from playlists 
            for(const playlistId of song.playlist){
                const playlist= await playlistModel.findById(playlistId);
                playlist.songs=playlist.songs.filter((curSong:any)=>curSong._id.toString()!=curSongId);
                await playlist.save(); 
            }
        }
        //delete songs with this artist
        for(const curSongId of artist.songs){
            await songModel.findByIdAndRemove(curSongId);
            }

        //delete artist
        await artistModel.findByIdAndRemove(artistId);

        return artist;
}
