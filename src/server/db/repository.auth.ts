import userModel from "../modules/user/user.model.js";


export async function getUser(nickname:string){
    return await userModel.findOne({"nickname":nickname});
}
