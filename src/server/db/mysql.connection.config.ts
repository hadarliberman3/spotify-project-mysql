import mysql from "mysql2/promise";
const {HOST,DB_USER,DB_PASSWORD,DB_NAME}=process.env;

export const config= {

    host: HOST,
    user: DB_USER,
    password:DB_PASSWORD,
    database: DB_NAME,
  
  };
// export async function connectMysql(){
//   const connection:any=await mysql.createConnection(config);
//   return connection.connect();
// }


export let connection: mysql.Connection;
export const connect_mysql = async () => {
    if (connection) return connection;
    connection = await mysql.createConnection({
      host: HOST,
      user: DB_USER,
      password:DB_PASSWORD,
      database: DB_NAME,
    });
    await connection.connect();
};
  
  

