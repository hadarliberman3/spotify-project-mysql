
import songModel from "../modules/song/song.model.js";
import artistModel from "../modules/artist/artist.model.js";
import playlistModel from "../modules/playlist/playlist.model.js";
import { Iuser,IeditUser } from "../modules/user/user.interface.js";
import userModel from "../modules/user/user.model.js";

export async function createUserDB(user:Iuser){
    return await userModel.create(user);
     
}

export async function getAllUsersDB (){
    return await userModel.find()
    .populate("playlist","-_id name");
    
}

export async function getOneUserDB(userId:string){
    const user = await userModel.findById(userId)
    .populate("playlist","-_id name");
    return user;
}

export async function updateUserDB(userId:string,user:IeditUser){
    return await userModel.findByIdAndUpdate(userId,user,{new: true, upsert: false });
}

export async function addPlaylistToUserDB(userId:string,playlistId:string){
    const playlist = await playlistModel.findById(playlistId);
    const user= await userModel.findById(userId);

    //add song to pallyist
    //add playlist to user
    if(!user.playlist_list.includes(playlistId)){
        user.playlist_list.push(playlist);
        await user.save();
    }

    //add playlistId to song
    //add userId to playlist
    if(!playlist.users.includes(userId)){
        playlist.users.push(userId);
        await playlist.save();
    }
    return user;
}

export async function deleteUserDB(userId:string){

        const user = await userModel.findByIdAndRemove(userId);
        const artist=await artistModel.findById(user.artist);
    
        //delete from artist
        artist.users=artist.users.filter((cur:string)=>{
            console.log(cur,userId);
            return cur!=userId;
        });
        await artist.save();

        //delete from playlists
        for(const curPlaylistId of user.playlist){
            const playlist= await playlistModel.findById(curPlaylistId);
            playlist.users=playlist.users.filter((curUser:any)=>curUser._id.toString()!=userId);
            await playlist.save();
        }
        
        return user;
}

export async function deleteRefreshTokenDB(userId:string){
    const user = await userModel.findById(userId);
    user.refresh_token="";
    await user.save();

}
