
import { Request,Response,NextFunction } from "express";
import jwt from "jsonwebtoken";
import { JwtPayload } from "jsonwebtoken";
import {getRolesFromAccessToken} from "../modules/auth/auth.service.js";

const {APP_SECRET}=process.env;
export const verifyAuth = async (req:Request, res:Response, next:NextFunction) => {
    try {     
        // check header or url parameters or post parameters for token
        const access_token = req.headers["x-access-token"];
  
        if (!access_token) return res.status(403).json({
            status:"Unauthorized",
            payload: "No token provided."
        });
  
        // verifies secret and checks exp
        const decoded = await jwt.verify(access_token as string, APP_SECRET as string);
  
        // if everything is good, save to request for use in other routes
        const {username} = decoded as JwtPayload;
        req.user_id = username;
        next();
  
    } catch (error) {
        return res.status(401).json({
            status:"Unauthorized",
            payload: "Unauthorized - Failed to authenticate token."
        });
    }
  };

  export function verifyAuthRole(authRoles:string[]){
       return async function (req:Request, res:Response, next:NextFunction) {
    try {     
       
        const access_token = req.headers["x-access-token"];
        if (!access_token) return res.status(403).json({
            status:"Unauthorized",
            payload: "No token provided."
        });

        const userRoles= await getRolesFromAccessToken(access_token as string);
        console.log(userRoles);
        console.log(authRoles);
        console.log(userRoles.some((item:string) => authRoles.includes(item)));
        if(userRoles.some((item:string) => authRoles.includes(item))){
            
            next();
        }
        else{
            return res.status(401).json({
                status:"Unauthorized",
                payload: "Unauthorized - you dont have permission to do it."
            });

        }
    }
    catch(err){
        console.log(err);
    }
    };
  }
  

  
 
